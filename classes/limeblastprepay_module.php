<?php

class LimeBlastPrepay_Module extends Core_ModuleBase {

	/**
	 * Creates the module information object
	 * @return Core_ModuleInfo
	 */
	protected function createModuleInfo()
	{
		return new Core_ModuleInfo(
				"Prepay Payment Gateway",
				"Adds an account code based prepay payment gateway to the site",
				"The Lime Blast Collective");
	}

	/**
	 * Subscribe to system events to add your own behaviour
	 */
	public function subscribeEvents()
	{
		Backend::$events->addEvent('shop:onExtendCustomerModel', $this, 'extend_customer_model');
		Backend::$events->addEvent('shop:onExtendCustomerForm', $this, 'extend_customer_form');
	}


	public function extend_customer_model($customer)
	{
		$customer->define_column('x_limeblastprepay_credit', 'Credit available')->currency(true)->validation()->fn('trim');
	}

	public function extend_customer_form($customer)
	{
		$customer->add_form_field('x_limeblastprepay_credit')->tab('Prepay Credit');
	}

}