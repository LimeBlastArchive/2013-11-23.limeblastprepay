<?php

class LimeBlastPrepay_Prepay_Payment extends Shop_PaymentType {

	public function get_info()
	{
		return array(
			'name' => 'Customer Prepay',
			'description' => 'Allows for the use of customer\'s prepay credit for payments'
		);
	}

	public function build_config_ui($host_obj, $context = null)
	{
		$host_obj->add_field('order_status', 'Order Status')->tab('Configuration')->renderAs(frm_dropdown)->
			comment('Select status to assign the order in case of successful payment.', 'above', true);
	}

	public function get_order_status_options($current_key_value = -1)
	{
		if ($current_key_value == -1)
			return Shop_OrderStatus::create()->order('name')->find_all()->as_array('name', 'id');

		return Shop_OrderStatus::create()->find($current_key_value)->name;
	}

	public function validate_config_on_save($host_obj)
	{
		// this page has been left blank intentionally
	}

	public function status_deletion_check($host_obj, $status)
	{
		if ($host_obj->order_status == $status->id)
			throw new Phpr_ApplicationException('This status cannot be deleted because it is used in the prepay payment method.');
	}

	public function build_payment_form($host_obj)
	{
		// this page has been left blank intentionally
	}

	private function prepare_fields_log($fields)
	{
		return $fields;
	}

	public function process_payment_form($data, $host_obj, $order, $back_end = false)
	{
		try
		{
			// get customer info
			$customer = Cms_Controller::get_customer();

			// check customer is logged in
			if (!$customer) {
				// customer not logged in, throw exception
				throw new Exception('Not logged in');
			}

			// check the user has enough funds
			if ($order->total > $customer->x_limeblastprepay_credit) {
				// Customer doesn't have enough funds, throw exception
				throw new Exception('Not enough credit');
			}

			// so far so good, lets deduct the credit from their account
			$new_credit_amount = $customer->x_limeblastprepay_credit - $order->total;
			$customer->x_limeblastprepay_credit = $new_credit_amount;
			$customer->save();

			// payment successful, let lemonstand do its thing
			$this->log_payment_attempt($order, 'Successful payment', 1, array(), array(), 'Prepayment successful');
			Shop_OrderStatusLog::create_record($host_obj->order_status, $order);
			$order->set_payment_processed();
		}
		catch (Exception $ex)
		{
			// payment failed
			$this->log_payment_attempt($order, $ex->getMessage(), 0, array(), array(), null);
			throw new Phpr_ApplicationException($ex->getMessage());
		}
	}

}