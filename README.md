Customer-based prepay module for Lemonstand
===========================================

A custom module for use in Lemonstand to provide a customer-based prepay payment method.

It allows administrators to assign credit to customers, which they're then able to use for payment by selecting the prepay payment method when checking out.

About the module
----------------

The module is very simple, using a single additional field (`x_limeblastprepay_credit`) in the customers table to keep track of credit available. Upon a successful purchase, provided there are enough funds, the gateway will deduct the order total from field, and mark the order as paid.

**This module is not supported!**

I've released it into the wild in the hope that it might be helpful to someone else. I have no plans to update this any further, but this might change as requirements for the project it was built for change.  A [development thread](http://forum.lemonstandapp.com/topic/4332-pre-pay-payment-module/) can be found in the Lemonstand forums.

How to install
--------------

* Clone/download the `limeblastprepay` folder into the `/modules/` folder.
* Log out and then back into admin area.
* Add prepay as a payment method.
* (optional) use `<?= format_currency($this->customer->x_limeblastprepay_credit) ?>` somewhere in your template to show much much credit the logged in user has available.

How to use
----------

* Find and edit a customer using the Lemonstand admin area to add credit to their account.
* When checking out, the customer must select the prepay payment method.

Please note
-----------

* The customer must have enough credit in their account to cover the entire transaction.
* The customer must be logged in while viewing [the pay page](http://lemonstand.com/docs/pay_page/) for the module to work (this can be enforced by setting the security tab on the Pay page to *Customers only*).